channelape-faker

# Setup
1. Open up **src/config**
1. Add your API key (or session id) on line 3 **config.channelApeSecretKey**
1. Find **businessChannelChoices** on line 11
    1. This is an array made up of objects with the following properties:
        * **name**: This is an arbitrary human readable value that you can use to easily distinguish what you have set up for this option.
        * **value.businessId**: Business ID of the ChannelApe business you want to prep orders on.
        * **value.channelId**: Channel ID on the business denoted previously that you want orders associated with.
        * **value.identifier**: Same thing as **name**
        * **value.ordersFile**: This file (relative to **src/resources/orders**) will be read when creating orders. With this option you can reset orders to the state they are in that file as many times as you like.
    1. Adjust those values to whatever you'd like
1. Then just run `npm i` followed by an `npm start`

## Tips
1. As is, this script will **delete** orders on the channelId / businessId combination you have selected. If you want to keep adding orders instead of clearing out the ones currently on that channel simply comment out line 30 in **src/service/order-prepper-service.js**
1. If you want to create a copy of production orders, it's simple enough. Just copy and paste the results from a POSTMAN call to orders and put the results in an order file and update your config to point to the new file.
1. Do you want **RANDOM** orders? After starting the script it will ask you if you want to make random orders, simply choose yes and then input how many random orders you wish to create.
    1. Locate the **getFakeOrder()** function in **order-prepper-service.js**. This is currently a fairly rudimentary function but you can simply add logic in here utilizing [faker](https://www.npmjs.com/package/faker).
    1. Faker is really fun, try using `faker.commerce.productName()`. It's hilarious.
1. The fake order generator can be trivially updated to use the catalog on whatever business as the SDK we have already can search variants and grab random ones. This way we can have images on orders and products that make actual sense.