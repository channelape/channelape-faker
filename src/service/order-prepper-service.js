const Q = require('q');
const log4js = require('log4js');
const request = require('request');
const ProgressBar = require('progress');
const faker = require('faker');
const random = require('random');
const fs = require('fs');

const barTokenString = ':bar :percent (:current)';

module.exports = OrderPrepperService;

function OrderPrepperService(endpoint, channelApeSecretKey, channelApeClient) {
  const logger = log4js.getLogger('OrderPrepperService');
  const service = {
    prepareOrders
  };
  return service;

  function prepareOrders(channelId, businessId, orders) {
    if (typeof orders === 'number') {
      orders = getFakeOrders(orders, channelId);
    } else if (!Array.isArray(orders)) {
      orders = orders.orders;
    }
    fs.writeFileSync('orders.json', JSON.stringify(orders));
    const createBar = new ProgressBar(barTokenString, { total: orders.length });
    const deferred = Q.defer();
    getCurrentOrders()
      .then(deleteOrders)
      .then(createOrders)
      .then((response) => {
        logger.info(`Created ${response.length} orders`);
        logger.info('All done prepping orders');
        deferred.resolve(response);
      })
      .catch((e) => {
        logger.error(e);
        deferred.reject(e);
      });
    return deferred.promise;

    function getCurrentOrders() {
      return channelApeClient.orders().get({ businessId });
    }

    function deleteOrders(currentOrders) {
      const deletionPromises = [];
      for (let i = 0; i < currentOrders.length; i++) {
        deletionPromises.push(callDelete(currentOrders[i]));
      }
      const deleteBar = new ProgressBar(barTokenString, { total: deletionPromises.length });
      logger.info(`Deleting ${deletionPromises.length} order(s)`);
      return Q.all(deletionPromises);

      function callDelete(order) {
        return channelApeClient.orders().delete(order.id)
          .then((deletedOrder) => {
            deleteBar.tick();
            return Promise.resolve(order);
          });
      }
    }

    function createOrders() {
      const creationPromises = [];
      for (let i = 0; i < orders.length; i++) {
        creationPromises.push(callCreate(orders[i]));
      }
      logger.info(`Creating ${creationPromises.length} order(s)`);
      return Q.all(creationPromises);

      function callCreate(order) {
        delete order.id;
        order.businessId = businessId;
        order.channelId = channelId;
        // order.fulfillments = [];
        // order.status = 'OPEN';
        // order.channelOrderId = order.channelOrderId.substring(0, 20);
        return channelApeClient.orders().create(order)
          .then((createdOrder) => {
            createBar.tick();
            return Promise.resolve(createdOrder);
          });
      }
    }
  }

  function getFakeOrders(count, channelId) {
    const orders = [];
    const variants = JSON.parse(fs.readFileSync('./src/resources/variants/steve-madden/all-variants.json', 'utf-8'));
    for (let i = 0; i < count; i += 1) {
      orders.push(getFakeOrder(channelId, variants));
    }
    return orders;
  }

  function getFakeOrder(channelId, variants) {
    const expectedFirstName = faker.name.firstName();
    const expectedLastName = faker.name.lastName();
    const expectedChannelOrderId = random.int(11111, 99999);
    const fullName = `${expectedFirstName} ${expectedLastName}`;
    const expectedOrderStatus = 'OPEN';
    const expectedPurchasedAtDate = new Date();

    const orderToCreate = {
      additionalFields: [
        { name: 'name', value: `SDK${parseInt((Math.random() * 100000).toString(), 10).toString()}` },
        { name: 'order_number', value: parseInt((Math.random() * 100000).toString(), 10).toString() }
      ],
      totalPrice: faker.random.number({ min: 1, max: 700, precision: 2 }),
      alphabeticCurrencyCode: 'USD',
      channelId: channelId,
      channelOrderId: expectedChannelOrderId,
      customer: {
        firstName: expectedFirstName,
        lastName: expectedLastName,
        name: fullName,
        email: faker.internet.email(),
        additionalFields: [
          { name: 'extraCustomerData', value: faker.random.words(5) },
          { name: 'id', value: `${random.int(1000, 9999)}` }
        ],
        shippingAddress: {
          address1: faker.address.streetAddress(),
          city: faker.address.city(),
          country: 'United States',
          countryCode: 'US',
          firstName: expectedFirstName,
          lastName: expectedLastName,
          name: fullName,
          postalCode: faker.address.zipCode(),
          provinceCode: faker.address.stateAbbr(),
          province: faker.address.state(),
        }
      },
      status: expectedOrderStatus,
      purchasedAt: expectedPurchasedAtDate,
      lineItems: []
    };
    for (let i = 0; i < random.int(1, 5); i += 1) {
      orderToCreate.lineItems.push({
        id: (i + 1).toString(),
        quantity: random.int(1, 25),
        title: faker.commerce.productName(),
        sku: faker.random.alphaNumeric(random.int(4, 8)),
        additionalFields: [
          { name: 'extraLineItemData', value: faker.random.words(5) }
        ]
      });
    }
    return orderToCreate;
  }
}
