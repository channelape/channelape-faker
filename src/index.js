const config = require('./config');
const inquirer = require('inquirer');
const log4js = require('log4js');
const fs = require('fs');
const OrderPrepperService = require('./service/order-prepper-service');
const ChannelApeClient = require('channelape-sdk').ChannelApeClient;

const channelApeSecretKey = config.channelApeSecretKey;
require('console.table');

let businessId = '';
let channelId = '';
let businessAndChannelIdentifier = '';
let ordersFile = '';
let numberOfRandomOrders = 0;

const logger = log4js.getLogger('APP');
log4js.setGlobalLogLevel('ALL');

const ordersClient = new ChannelApeClient({
  sessionId: channelApeSecretKey,
  endpoint: config.channelApeApiDomainName,
  minDelay: 1000,
  maxAttempts: 30,
  base: 1000,
  bucketSize: 240,
  bucketIntervalSeconds: 60,
  bucketMaxWait: 600
});

const orderPrepperService = new OrderPrepperService(config.channelApeApiDomainName, channelApeSecretKey, ordersClient);

const PREP_AGAIN = 1;
const QUIT = 3;

if (config.channelApeSecretKey.trim() === '') {
  logger.error('Open ./config.js and supply your channel ape secret key.\r\n');
  process.exit();
}

const prepQuestions = [
  {
    name: 'businessAndChannel',
    type: 'list',
    message: 'Prep orders for:',
    choices: config.businessChannelChoices
  }, {
    name: 'randomOrders',
    type: 'confirm',
    message: 'Create random orders?',
    default: false
  }, {
    name: 'numberOfRandomOrders',
    type: 'input',
    message: 'How many random orders should be created?',
    when: (answers) => {
      return answers.randomOrders;
    }
  }
];

const nextActionQuestions = [
  {
    name: 'nextAction',
    type: 'list',
    message: 'What would you like to do next?',
    choices: [
      {
        name: `Prep orders again for ${businessAndChannelIdentifier}`,
        value: PREP_AGAIN
      },
      {
        name: 'Quit',
        value: QUIT
      }
    ]
  }
];

inquirer.prompt(prepQuestions)
  .then((answers) => {
    businessId = answers.businessAndChannel.businessId;
    channelId = answers.businessAndChannel.channelId;
    businessAndChannelIdentifier = answers.businessAndChannel.identifier;
    ordersFile = answers.businessAndChannel.ordersFile;
    if (answers.randomOrders) {
      numberOfRandomOrders = parseInt(answers.numberOfRandomOrders);
    }
    nextActionQuestions[0].message = `Reset orders on ${businessAndChannelIdentifier.yellow}?`;
  })
  .then(prepareOrders)
  .catch(errorHandler);

function prepareOrders() {
  const orders = JSON.parse(fs.readFileSync(`./src/resources/orders/${ordersFile}`));
  orderPrepperService.prepareOrders(
    channelId,
    businessId,
    numberOfRandomOrders > 0 ? numberOfRandomOrders : orders
  )
    .then(promptForNextAction)
    .catch(errorHandler);
}

function promptForNextAction() {
  inquirer.prompt(nextActionQuestions)
    .then((answers) => {
      switch (answers.nextAction) {
        case (PREP_AGAIN):
          prepareOrders();
          break;
        case (QUIT):
          console.log('\r\n');
          process.exit();
          break;
        default:
          process.exit();
          break;
      }
    });
}

function errorHandler(e) {
  if (Array.isArray(e) && e[0].hasOwnProperty('code')) {
    logger.error('An error occurred:');
    console.table(e);
  } else {
    logger.error(e);
  }
}
